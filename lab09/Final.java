// Nicola Chomiak
/// November 15, 2018
/// CSE 002 310
public class Final{
public static void main (String args []){
  int [] myArray = {1,2,3,4,5,6,7,8,9}; // literal int array declared
  int [] copyArray = copy(myArray);
  Inverter(copyArray);
  
  int [] array0 = myArray;
  int [] array1 = copy(myArray);
  int [] array2 = copy(myArray);
  //pass array0 to inverter() and print it out
  print(array0); // double check if this accurately passing 
  Inverter(array0);
  print(array0);
  //pass array1 to inverter2()
  Inverter2(array1);
  print(array1); // double check if this accurately passing 
// pass array2 to inverter2() and then assign to value to array3
  int[] array3 = Inverter2(array2);
  print(array3);
}


  // create a method called copy()
public static int[] copy(int[] inputArray){
  int []copyArray = new int[inputArray.length];
  for (int i = 0; i < inputArray.length; i++){
    copyArray[i] = inputArray[i];
   }
    return copyArray;
}
  
public static void Inverter(int[] inputArray){
    for (int i =1; i <= inputArray.length/2; i++){
      int lastValue = inputArray[inputArray.length - i];
      int tempValue = inputArray[i-1];
      inputArray[i-1] = lastValue;//inputArray[inputArray.length - i];
      inputArray[inputArray.length - i] = tempValue;
    }
  }
  public static int [] Inverter2(int[] myArray){
    int [] inputArray = copy(myArray);
   for (int i =1; i <= inputArray.length/2; i++){
      int lastValue = inputArray[inputArray.length - i];
      int tempValue = inputArray[i-1];
      inputArray[i-1] = lastValue;//inputArray[inputArray.length - i];
      inputArray[inputArray.length - i] = tempValue;
    }
    return inputArray;
  }
  public static void print(int[] myArray)
  {
    for (int i = 0; i < myArray.length; i++)
    {
      System.out.print(myArray[i] + " ");
    }
    System.out.println();
  }
}


