
Grading Sheet for HW8

Grade: 92/100

Compiles    				20 pts
Comments 				5 pts
Method shuffle(list)			20 pts
Method getHand(list,index, numCards)  20 pts
Method printArray(list).               20 pts
Checks If numCards given is greater than the number of cards in the deck and if so creates a new deck of cards. 7 pts // you do this part but you do not print out the new deck 