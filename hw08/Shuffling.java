/// Nicola Chomiak
/// November 13, 2018
/// CSE 002 310
import java.util.ArrayList;
import java.util.Scanner;
public class Shuffling{
  public static void main(String [] args){
    Scanner scan = new Scanner( System.in);
    //suits club, heart, spade or diamond
    String[] suitNames={"C","H","S","D"};
    String[] rankNames={"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
    String[] cards = new String[52];
    String[] hand = new String[5];
    int numCards = 5;
    int again = 1;
    int index = 51;
    for (int i=0; i<52; i++){
      cards[i]=rankNames[i%13]+suitNames[i/13];
      System.out.print(cards[i]+ " ");
    }
  System.out.println();
   printArray(cards);
 shuffle(cards);
  printArray(cards);
 while(again == 1){
   hand = getHand(cards,index,numCards);
    printArray(hand);
      index = index - numCards;
     System.out.println("Enter a 1 if you want another hand drawn");
    again = scan.nextInt();
    }
  }
    
// for (int i = 0; i < cards.length; i++) //initializes the cards
//   cards[i] = i;
    public static void printArray(String [] list){
      for (int i = 0; i<list.length; i++){
        System.out.print(list[i]+ " ");
  }
      System.out.println();
    }
    public static String [] shuffle (String [] list){
    for (int i = 0; i < list.length; i++){
      String [] temp = new String [list.length]; 
      // generate index randomly
      int index = (int)(Math.random()*list.length);
      temp[0] = list [0];
      list[0] = list[index];
      list[index] = temp[0];
    }
      return list;
    }
   public static String [] getHand (String [] list, int index, int numCards) {// display the first 5 cards
   String [] hand = new String [numCards];
       if (numCards > index+1){    
      list = shuffle(list);
         index = 51;
    }
      int counter = 0;
     for (int i = (index - numCards+1); i <= index; i++){  
       hand[counter] = list[i];
       counter++;
     }
      counter = 0;
      return hand;
     
    }
    }
  
