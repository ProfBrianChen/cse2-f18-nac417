/////////////
///// Nicola Chomiak, HW 3, CSE 02
///
import java.util.Scanner;

public class Pyramid{
  
  public static void main(String args[]){
    Scanner myScanner = new Scanner (System.in);
    System.out.println("The square side of the pyramid is (input length):");
    double length = myScanner.nextDouble() ;
    System.out.println("The height of the pyramid is (input height)");
    double height = myScanner.nextDouble() ; 
    double bottomarea = length * length;
    double volume = bottomarea * height / 3;
    System.out.println("The volume inside the pyramid is:" + volume);
    
  }
}