import java.util.Scanner;
// Nicola Chomiak, CSE 002 -310, September 13 2018
//
//
public class Check{
  // main method required for every Java program
  public static void main (String[] args) {
    
    Scanner myScanner = new Scanner( System.in ) ; // doing this accepts the input and constructs the Scanner that I declared earlier
    System.out.print("Enter the original cost of the check in the form xx.xx: ") ; // this prompts the user for the original cost of the check
    double checkCost = myScanner.nextDouble() ; // this accepts the user input by using the statement 
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // this prompts the user for the tip percentage that they wish to pay and accept the input
    double tipPercent = myScanner.nextDouble() ;
    tipPercent /= 100 ; // this will convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ") ;
    int numPeople = myScanner.nextInt() ; // this prompts the user for the number of people that went to dinner and accept the input. this is also the number of ways the check will be split up.int
    double totalCost; //now I am starting to print the output so this line is for the total cost
    double costPerPerson; // this is the cost per person
    int dollars, //whole dollar amount of cost
        dimes, pennies; //for storing digits
                        //to the right of the deimal point
                        //for the cost$
    totalCost = checkCost * (1 + tipPercent) ; //outputs the total cost
    costPerPerson = totalCost / numPeople; //outputs the cost per person
    //get the whole amount, dropping decimal fraction dollars=(int)costPerPerson;
    //get dimes amount, e.g.,
    // (int) (6.73 * 10) % 10 -> 67 % 10 -> 7
    //where the % (mod) operator returns the remainder
    //after the division: 583%100 -> 83, 27%5 -> 2
    dimes=(int) (costPerPerson *10) % 10;
    pennies=(int) (costPerPerson *100) % 10;
    dollars=(int) costPerPerson ;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies) ; //tells us how much each person is paying towards the total of the check
    
    
    
  } //end of main method
} //end of class