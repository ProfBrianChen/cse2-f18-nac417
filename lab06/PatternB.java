// Nicola Chomiak
/// CSE 002 310
/// October 11, 2018
import java.util.Scanner;
public class PatternB{
  public static void main (String [] args){
    Scanner scnr = new Scanner (System.in);
    int i,m;
    int numRows;
      numRows = 0;
    while (numRows >= 0){
      System.out.print("Enter an integer between 1 and 10:");
      numRows = scnr.nextInt();
      for (int ii = numRows; ii >= 1; ii -- ){
        for (int mm = 1; mm <= ii ; mm ++ ){
          System.out.print(mm + " ");
        }
        System.out.println();
        scnr.close();
      }
    }
  }
}