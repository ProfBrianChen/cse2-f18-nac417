//////// In this program I will be creating a random number generator to "try to practice card tricks".In
/// Nicola Chomiak, CSE 002 Card Generator, September 20, 2018
////
public class CardGenerator{
  
  public static void main(String args[]){ //starts the code
    System.out.println("Pick a card number 1-52.");
     // creates a random number generator and chooses a number between 1-52
    int card = (int)(Math.random()*(52+1))+1;
//     int cardNum = (int)(Math.random()*(13+1)+1;
    String suitname= "";
    if (card>= 1 && card <= 13){
      suitname = "Diamonds";
    }
    if (card>= 14 && card <= 26){
      suitname = "clubs";
    }
    if (card>=27 && card <= 39){
      suitname = "hearts";
    }
    if (card>= 40 && card <= 52){
      suitname = "spades";
    }
    
    int cardNum = card%13;
   String cardString = "";
    switch (cardNum) {
      case 1: cardString = "Ace";
      break;
      case 2: cardString = "2";
      break;
      case 3 : cardString = "3";
      break;
      case 4 : cardString = "4";
      break;
      case 5 : cardString = "5";
      break;
      case 6 : cardString = "6";
      break;
      case 7 : cardString = "7";
      break;
      case 8 : cardString = "8";
      break;
      case 9 : cardString = "9";
      break;
      case 10 : cardString = "Jack";
      break;
      case 11 : cardString = "Queen";
      break;
      case 12 : cardString = "King";
      break;
      case 13 : cardString = "Joker";
      break;
    }
    
 System.out.println("You picked the " + cardString + " of " + suitname + ".");
    
  }
}