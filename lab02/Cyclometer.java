// adding these two dashes allows you to put notes in your code while your going and won't show up when you actually run the code so you can go back and explain why you did what you did! 
//////////
public class Cyclometer {
      // main method required for every Java program
      public static void main (String [] args)      {
        int secsTrip1=480; // seconds per trip 1
        int secsTrip2=3220; // seconds per trip 1
        int countsTrip1=1561; // number of counts for trip 1
        int countsTrip2=9037; // number of counts for trip 2
        // our intermediate variables and output data
        double wheelDiameter=27.0, // diameter of the bicycle wheel
        PI=3.1459, // the value of PI
        feetPerMile=5280, // number of feet per mile
        inchesPerFoot=12, // inches per foot
        secondsPerMinute=60; // number of seconds per minute
        double distanceTrip1, distanceTrip2, totalDistance; // total distance of trip 1 and trip 2
        System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
        System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
        // run the calculations; store the values. Document my calculation here. What am I calculating?
        //
        //
        distanceTrip1=countsTrip1*wheelDiameter*PI;
        // Above gives distance in inches
        // (for each count, a roation of the wheel travels the diameter in inches times PI)
        distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
        distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
        totalDistance=distanceTrip1+distanceTrip2;
        //Print out the output data.
        System.out.println("Trip 1 was "+distanceTrip1+" miles");
        System.out.println("Trip 2 was "+distanceTrip2+" miles");
        System.out.println("The total distance was "+totalDistance+" miles");
        
      } //end of main method
}   //end of class