//
/////// Nicola Chomiak 
public class Arithmetic{
  public static void main (String args[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belts
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    double costPants = (int) ((numPants * pantsPrice) * 100); //total price of pants
    costPants = costPants / 100; //converts to 2 decimal places
    
    double costShirts = (int) ((numShirts * shirtPrice) *100); //total price of sweatshirts
    costShirts = costShirts / 100; //converts to 2 decimal places
    
    double costBelt = (int) ((beltCost * numBelts) *100); //total price of belts
    costBelt = costBelt / 100; //converts to 2 decimal places
    
    double taxPants = (int) ((costPants * paSalesTax) * 100); //tax on pants
    taxPants = taxPants / 100; //converts to 2 decimal places
    
    double taxShirts = (int) ((costShirts *paSalesTax) * 100); //tax on shirts
    taxShirts = taxShirts / 100; //converts to 2 decimal places 
    
    double taxBelt = (int) ((beltCost * numBelts) * 100); //tax on belt
    taxBelt = taxBelt / 100; //onverts to 2 decimal places
    
    double subTotal = costPants + costShirts +costBelt; //subtotal of items before tax
    
    double totalTax = taxPants + taxShirts + taxBelt; //total tax collected
    double grandTotal = subTotal + totalTax; //grand total of transcration
    
    System.out.println ("Cost of Pants: $" + costPants); //prints cost of pants
    System.out.println ("Cost of Sweatshirts: $" + costShirts); //prints cost of pants
    System.out.println ("Cost of Belt: $" + costBelt); //prints cost of belt
    System.out.println ("Tax on Pants: $" + taxPants); //prints tax on pants
    System.out.println ("Tax on Sweatshirts: $" + taxShirts ); //prints tax on shirts
    System.out.println ("Tax on Belt: $" + taxBelt); //prints tax on belt
    
    System.out.println ("Subtotal: $" + subTotal); //prints subtotal
    System.out.println ("Total Tax: $" + totalTax); //prints total tax
    System.out.println ("Grand Total: $" + grandTotal); //print grand total
    
    
  }
}