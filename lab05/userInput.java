// Nicola Chomiak
/// CSE 002 310
/// October 4, 2018

import java.util.Scanner;
public class userInput {
  public static void main (String [] args){
    Scanner myScanner = new Scanner (System.in);
    
    int courseNum;
    String departmentName;
    int coursefrequencyNum;
    int courseTime;
    String instructorName;
    int studentsinclassNum;
    
    System.out.print("Enter your course number");
     boolean correct = myScanner.hasNextInt();
    while(correct == false){
      System.out.println("false input, input again");
      myScanner.next();
      correct  = myScanner.hasNextInt();
    }
    courseNum = myScanner.nextInt();
    System.out.println("Course num is: " + courseNum);
    
    System.out.print("Enter your department name");
    while (correct == false){
      System.out.println("false input, input again");
      myScanner.next();
      correct = myScanner.hasNext();
    }
    departmentName = myScanner.next();
    System.out.println("Department name is:" +departmentName);
    
    System.out.print("Enter the number of times the course meets in a week");
    while(correct == false){
      System.out.println("flase input, input again");
      myScanner.next();
      correct = myScanner.hasNextInt();
    }
    coursefrequencyNum =myScanner.nextInt();
    System.out.println("Number of times the course meets in a week is " + coursefrequencyNum);
    
    System.out.print("Enter the time the course meets in military time with no colons");
    while(correct == false){
      System.out.println("false input, input again");
      myScanner.next();
      correct = myScanner.hasNextInt();
    }
    courseTime = myScanner.nextInt();
    System.out.println("The course meets at " + courseTime);
    
    System.out.print("Enter the instructor's name");
    while (correct == false){
      System.out.println("false input, input again");
      myScanner.next();
      correct = myScanner.hasNext();
    }
    instructorName = myScanner.next();
    System.out.println("The instructor's name is " + instructorName);
    
    System.out.print("Enter the number of students in the class");
    while (correct == false){
      System.out.println("false input, input again");
      myScanner.next();
      correct = myScanner.hasNextInt();
    }
    studentsinclassNum = myScanner.nextInt();
    System.out.println("Students in the class:" + studentsinclassNum);
  }
    

  }

